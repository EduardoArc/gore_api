import { Sequelize } from 'sequelize-typescript';
import { Postulacion } from '../models/postulacion';
import { User } from '../models/user';

export const conection = new Sequelize({
    host: 'localhost',
    username: 'eduardoarce',
    database:'GORE_DB',
    dialect: 'postgres',
    password: '',
    models:[User, Postulacion],
    //Evita mostrar mensaje en consola
    logging:true
  });