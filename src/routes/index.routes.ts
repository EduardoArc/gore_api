import { Router, Request, Response } from "express";

import auth from './auth.routes'

const routes = Router();


routes.use("/api",auth);

export default routes;